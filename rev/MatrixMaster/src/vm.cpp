#include <stdio.h>
#include <stdint.h> // for specefic size ints
#include <string>
#include <string.h>
#include <stdlib.h>
#include<vector> 

#define IMPORT_DATA_AS_STRING(str_name, symbol_name) \
    extern "C" char symbol_name ## _start; \
    extern "C" char symbol_name ## _end; \
    const std::string str_name ( \
    &symbol_name ## _start, \
    &symbol_name ## _end - &symbol_name ## _start)

IMPORT_DATA_AS_STRING(bytecode, _binary_keycheck);


// Virtual Machine Implementation

class VM
{
    public:
    uint32_t reg[16]; // 16 registers will be present for our system
    uint32_t pc{}; // Intruction pointer or program counter
    std::vector<uint8_t> memory; // corresponds to memory

    bool exec_instr(); // reponsible for executing individual instructions
    bool keyCheck(char key[32]); // Checks if key given is correct

};





bool VM::exec_instr()
{
    switch(memory[pc])
    {
        


        case 0x00: {
            // XOR dst_reg,src_reg --------> pc pc+1, pc+2 (index to memories) -------> opcode reg1, reg2
            reg[memory[pc+1]] ^= reg[memory[pc+2]]; 
            pc+=3;
            return false; 
            // True when full instruction is complete, we don't know wether next instruction is end or not ........ so just continue
        }


        case 0x01: { // AND dst_reg,src_reg
            reg[memory[pc+1]] &= reg[memory[pc+2]];
            pc+=3;
            return false; 
        }


        case 0x02: { // OR dst_reg,src_reg
            reg[memory[pc+1]] |= reg[memory[pc+2]];
            pc+=3;
            return false; 
        }


        case 0x03: { // MOV dst_reg,src_reg
            
            reg[memory[pc+1]] = reg[memory[pc+2]];
            pc+=3;
            return false; 
        }


        case 0x04: { // MOVI dst_reg,int32
            memcpy(&reg[memory[pc+1]],&memory[pc+2],4); 
            // here you can probably use assignment, but memory element is 8 bit while register is 32bit

            pc+=6; // because 4 bytes were copied + 1 register + 1 opcode
            return false; 
        }
        
        case 0x05:{ // Leftshift dst_reg, int32
            reg[memory[pc+1]] <<= memory[pc+2];
            pc+=6;
            return false;
        }

        case 0x06:{ // Rightshift dst_reg, int32
            reg[memory[pc+1]] >>= memory[pc+2];
            pc+=6;
            return false;
        }

        case 0x07:{ // Add dst_reg, int32
            reg[memory[pc+1]] += memory[pc+2];
            pc+=6;
            return false;
        }
        case 0x08:{ // Sub dst_reg, int32
            reg[memory[pc+1]] -= memory[pc+2];
            pc+=6;
            return false;
        }
        
        case 0x09: { // Rand dst_reg, int 32
            reg[memory[pc+1]] = rand() % memory[pc+2];
            pc+=6;
            return false;

        }

        case 0xff: { // END
           return true;
        }


    }
    printf("Invalid Opcode");
    exit(1);
}




bool VM::keyCheck(char key[64])
{
    // we could store the key in the registers or in the memory. We choose registers
    // uint32 stores 4 bytes so ,  capable of storing 4 letters

    memcpy(&reg[0],key+0,4);
    memcpy(&reg[1],key+4,4);
    memcpy(&reg[2],key+8,4);
    memcpy(&reg[3],key+12,4);
    memcpy(&reg[4],key+16,4);
    memcpy(&reg[5],key+20,4);
    memcpy(&reg[6],key+24,4);
    memcpy(&reg[7],key+28,4);



    bool end = false; // set end as false so that initially the execution starts

    while(!end)
    {
        // for(int i=0;i<8;i++)
        // printf("r%i: %.8x | ",i,reg[i]);
        // printf("\n");
        // for(int i=8;i<16;i++)
        // printf("r%i: %.8x | ",i,reg[i]);
        // printf("\n\n");
        end = exec_instr(); // runs until last instruction of VM is executed
    }

    return !(bool)reg[0]; 
    // we expect that the first register will hold the result of calculation
    // But the result of the calculation is 0 if true and 1 if false according to the keycheck.wasm
    // So there is a not operator before the 0th register

}





int main()
{

    srand(time(NULL));
    char key[64]{}; // this will set all the values to 0
    printf("Program Execution Starts here\n");
    printf("Enter the key : ");
    fflush(stdout);

    if(scanf("%32s",key) != 1)
    {
        printf("Error in Key\n");
        return 1;
    }

    VM vm;
    vm.memory.resize(4096);
    memcpy(&vm.memory[0],bytecode.data(),bytecode.size());

    bool isKeyValid = vm.keyCheck(key);

    if(isKeyValid)
    {
        printf("Nope");
    }
    else
    {
        printf("Nah");
    }
    return 0;


}


/* After writing this you need to make an assembler to feed the instructions into this VM, 
But making an assembler is tedious, so we will use netwide assmebler(nasm) to create macros and allow that
to assemble our code */

/* Code regarding that is present in vm.inc */
